package kz.baha.controllers;

import kz.baha.entities.FileEntity;
import kz.baha.repositories.FileEntityRepository;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
//import org.apache.commons

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by buffalobill571 on 5/13/17.
 */
@RestController
@RequestMapping(value = "/files")
public class FileEntityController {

    private FileEntityRepository fileEntityRepository;

    private final String spamPath = "/home/buffalobill571/IdeaProjects/fileuploadfinal/uploads/spam";

    @Autowired
    public FileEntityController(FileEntityRepository fileEntityRepository) {
        this.fileEntityRepository = fileEntityRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    List<FileEntity> getAll() {
        return this.fileEntityRepository.findAll();
    }

    @RequestMapping(value = "/bydate", method = RequestMethod.GET)
    List<FileEntity> getByDate(@RequestParam("date") String date) {
        DateFormat format = new SimpleDateFormat("yyyyy-mm-dd");
        try {
            Date sortDate = format.parse(date);
            return this.fileEntityRepository.findAll().stream()
                    .filter(f -> format.format(f.getDate()).equals(format.format(sortDate)))
                    .collect(Collectors.toList());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/spam", method = RequestMethod.GET)
    List<FileEntity> getSpam() {
        return this.fileEntityRepository.findAll().stream()
                .filter(f -> f.isSpam())
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/spam", method = RequestMethod.DELETE)
    @ResponseBody String deleteSpam() {
        this.fileEntityRepository.findAll().stream()
                .filter(f -> f.isSpam())
                .collect(Collectors.toList())
                .forEach(this.fileEntityRepository::delete);

        try {
            FileUtils.cleanDirectory(new File(spamPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "ok";
    }
}
