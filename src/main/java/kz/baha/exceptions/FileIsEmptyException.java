package kz.baha.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by buffalobill571 on 5/13/17.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FileIsEmptyException extends RuntimeException {

    public FileIsEmptyException() {
        super("file is empty");
    }
}
