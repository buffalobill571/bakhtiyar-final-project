package kz.baha.entities;

import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * Created by buffalobill571 on 5/13/17.
 */
public class FileEntity {

    @Id
    private String id;

    private boolean isSpam;
    private String fileName;
    private String path;
    private Date date;

    public FileEntity() {}

    public FileEntity(boolean isSpam, String fileName, String path, Date date) {
        this.isSpam = isSpam;
        this.fileName = fileName;
        this.path = path;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public boolean isSpam() {
        return isSpam;
    }

    public void setSpam(boolean spam) {
        isSpam = spam;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
