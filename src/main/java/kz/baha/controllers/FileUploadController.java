package kz.baha.controllers;

import kz.baha.entities.FileEntity;
import kz.baha.exceptions.FileBadExtensionException;
import kz.baha.exceptions.FileIsEmptyException;
import kz.baha.repositories.FileEntityRepository;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by buffalobill571 on 5/13/17.
 */
@RestController
@RequestMapping(value = "/final")
public class FileUploadController {

    private final String[] allowedExtensions = { "txt" };

    private final String path = "/home/buffalobill571/IdeaProjects/fileuploadfinal/uploads";
    private final String spamPath = "/home/buffalobill571/IdeaProjects/fileuploadfinal/uploads/spam";

    private FileEntityRepository fileEntityRepository;

    @Autowired
    FileUploadController(FileEntityRepository fileEntityRepository) {
        this.fileEntityRepository = fileEntityRepository;
    }

    @RequestMapping(method = RequestMethod.POST)
    FileEntity uploadFile(@RequestParam("text") MultipartFile multipartFile) {
        if (!multipartFile.isEmpty()) {
            try {

                String dirPath, name;

                if (!checkFileValidation(multipartFile)) throw new FileBadExtensionException();
                byte[] bytes = multipartFile.getBytes();

                dirPath = checkIfSpam(bytes) ? spamPath : generatePath();

                name = generateName(multipartFile, dirPath);

                BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(dirPath, name)));
                outputStream.write(bytes);
                outputStream.close();

                FileEntity entity = new FileEntity(checkIfSpam(bytes), name, dirPath, new Date());
                fileEntityRepository.save(entity);
                return entity;

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        } else throw new FileIsEmptyException();
    }

    private boolean checkIfFileExists(String basedir, String name) {
        File file = new File(basedir, name);
        return file.exists();
    }

    private String generateName(MultipartFile multipartFile, String path) {
        String name = multipartFile.getOriginalFilename().split("\\.")[0];
        String ext = multipartFile.getOriginalFilename().split("\\.")[1];
        while (true) {
            if (checkIfFileExists(path, name + "." + ext)) {
                name += RandomStringUtils.random(10, true, true);
            } else {
                break;
            }
        }
        return name + "." + ext;
    }

    private String generatePath() {
        DateFormat format = new SimpleDateFormat("yyyyy-mm-dd");
        File theDir = new File(path + "/" + format.format(new Date()));
        if (!theDir.exists()) theDir.mkdir();
        return theDir.getAbsolutePath();
    }

    private boolean checkIfSpam(byte[] bytes) {
        String s = new String(bytes);
        return s.toLowerCase().contains("spam");
    }

    private boolean checkFileValidation(MultipartFile multipartFile) {
        String ext = multipartFile.getOriginalFilename().split("\\.")[1];
        for (String extension:
                allowedExtensions) {
            if (ext.equals(extension)) return true;
        }
        return false;
    }
}
