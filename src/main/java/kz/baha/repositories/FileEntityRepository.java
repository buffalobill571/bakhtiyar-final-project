package kz.baha.repositories;

import kz.baha.entities.FileEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by buffalobill571 on 5/13/17.
 */
public interface FileEntityRepository extends MongoRepository<FileEntity, String> {


}
